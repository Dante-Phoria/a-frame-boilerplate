import { Entity } from "aframe";

let assets: Entity<any>;

/**
 * Load an asset file.
 * @param id ID to give the loaded asset.
 * @param filePath File path to asset file to load.
 * @returns ID to reference loaded asset in A-Frame. Parameter ID prepended with '#'.
 */
export function loadAsset(id: string, filePath: string): string
{
    const asset = document.createElement("a-asset-item");
    asset.setAttribute("id", id);
    asset.setAttribute("src", filePath);

    if (assets == null)
    {
        assets = document.querySelector("a-assets");

        if (assets == null)
        {
            const scene = document.querySelector("a-scene");
            assets = document.createElement("a-assets");
            scene.appendChild(assets);
        }
    }
    assets.appendChild(asset);

    return "#" + id;
}