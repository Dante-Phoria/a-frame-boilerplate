/**
 * Add an object as a child of another object and, optionally, give it attrubites.
 * @param type HTML tag name for type you want to add.
 * @param parent HTMLElement for this to be childed to.
 * @param attributes Optional. Array of tupled attribute names and values to add to the object.
 * @returns Created object.
 */
export function createObject(type: string, parent: HTMLElement, attributes?: Array<[attribute: string, value: string]>): HTMLElement
{
    const object = document.createElement(type);
    parent.appendChild(object);
    
    attributes?.forEach(element =>{
        object.setAttribute(element[0], element[1]);
    });
    
    return object;
}