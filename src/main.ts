// Import helpers.
import { createObject } from "./helpers/createObject";
import { loadAsset } from "./helpers/loadAsset";

// Import A-Frame.
require("aframe");
require("aframe-extras");

// When the webpage and imports finish loading...
window.onload = function()
{
    // Load a pre-made A-Frame scene and get a reference to the root scene object.
    document.body.innerHTML += require("./scenes/sampleScene.html");
    const scene = document.querySelector("a-scene");

    // Add a primative sphere to the scene.
    const basicSphere = createObject("a-sphere", scene);
    
    // Add a primative box to the scene and give it a position and rotation.
    const complexBox = createObject("a-box", scene, [
        ["position", "3, 2, 1"],
        ["rotation", "0, 0, 45"]
    ]);

    // Load an asset file for later use.
    const ninjaAsset = loadAsset("ninja", require("./assets/ninja.glb"));

    // Add an object to the scene as a child of the box and give it a position, scale, 3D model, and animation.
    const ninja = createObject("a-entity", complexBox, [
        ["position", "-3, 0, 0"],
        ["scale", "2, 2, 2"],
        ["gltf-model", ninjaAsset],
        ["animation-mixer", "clip: Idle"]
    ]);
}