# README #
<a name = "README"></a>

Made by Dante Cazley-Coldwell for use by [PHORIA](https://www.phoria.com.au/) Studios.

## Content ##

1. [What is this?](#What-is-this?)
2. [Version History](#Version-History)
3. [Setup](#Setup)
    1. [Development Environment](#Development-Environment)
    2. [Project](#Project "Project")
    3. [Running the Example](#Running-the-Example)
4. [Contact Us](#Contact-Us)
5. [Technical Documentation](#Technical-Documentation)


## What is this? ##
<a name = "What-is-this?"></a>

This is a boilerplate / starting point for making WebXR applications with ease using Webpack, A-Frame, and TypeScript.

It comes with many useful pre-configured commands and packages as well as a library of 'helpers' to keep code short, clean, and modular so you can get a quick start on making your WebXR experience without the long setup process of a new web project.

## Version History ##
<a name = "Version-History"></a>

Version | Changes
--------|--------
0.0.2   | Added aframe-extras package and loadAsset helper. Cleaned up some code and added commets to example to make them easier to understand. Set up Git LFS for *.glb and *.gltf asset files.
0.0.1   | Set up boilerplate with Webpack, A-Frame, and TypeScript. Includes a basic use example that imports a sample scene and adds a couple extra objects.

## Setup ##
<a name = "Setup"></a>

### Development Environment ###
<a name = "Development-Environment"></a>

1. Install [Visual Studio Code](https://code.visualstudio.com).

    1. Install the Visual Studio Code [Jira and Bitbucket (Official)](https://marketplace.visualstudio.com/items?itemName=Atlassian.atlascode) plugin.
    
    2. If the [Jira and Bitbucket (Official)](https://marketplace.visualstudio.com/items?itemName=Atlassian.atlascode) plugin didn't automatically install the [YAML](https://marketplace.visualstudio.com/items?itemName=redhat.vscode-yaml) plugin, install it manually.

2. Install [Node](https://nodejs.org/en/).

3. If you're on Windows, set your Current User Execution Policy to Remote Signed by opening Windows PowerShell and entering the command `Set-ExecutionPolicy -ExecutionPolicy RemoteSigned -Scope CurrentUser`. This is needed for some terminal commands to work later. You can read more about this on the [About Execution Policies](https://docs.microsoft.com/en-us/powershell/module/microsoft.powershell.core/about/about_execution_policies?view=powershell-7.1) page of the [Microsoft Documentations](https://docs.microsoft.com/en-us/) site.

### Project ###
<a name = "Project"></a>

1. Clone the repository. If you have installed the Visual Studio Code [Jira and Bitbucket (Official)](https://marketplace.visualstudio.com/items?itemName=Atlassian.atlascode) plugin you can clone directly to Visual Studio Code.

2. Open the repository folder in Visual Studio Code. If you cloned directly to Visual Studio Code, the repository should already be open.

3. Open the *webpack.config.js* file and change the HTMLWebpackPlugin title from A-Frame Boilerplate to your project name.

4. Open the *package.json* file and change the name from a-frame-boilerplate to your project name, and the author from Dante Cazley-Coldwell to your name.

5. In Visual Studio Code, open a terminal by navigating to Terminal -> New Terminal or, by default, pressing **Ctrl+Shift+`**

6. In the terminal, input the command `npm install` to initialize the packages this boilerplate uses. These do not come pre-initialized as it generates a large number and size of files in a folder called *node_modules*.

### Running the Example ###
<a name = "Running-the-Example"></a>

1. In Visual Studio Code, open a terminal by navigating to Terminal -> New Terminal or, by default, pressing **Ctrl+Shift+`**

2. In the terminal, input the command `npm start` to start a local server and run (serve) the *main.ts* file.

3. In the terminal, look for a line that says "Project is running at **Your server's local address**". E.g. http://localhost:8080/

4. On any device connected to the same network as the computer running your project, open a web browser and type your server's local address into the address bar. 

## Contact Us ##
<a name = "Contact-Us"></a>

Contact me at dante@phoria.com.au or via a direct message in the [PHORIA](https://www.phoria.com.au/) [Slack](https://slack.com/intl/en-au/) workspace.

[Back to top](#README)

# Technical Documentation #
<a name = "Technical-Documentation"></a>

## Content ##
1. [Terminal Commands](#Terminal-Commands)
1. [Packages](#Packages)
2. [Helpers](#Helpers)
    1. [Classes](#Classes)
    2. [Functions](#Functions)

## Terminal Commands ##
<a name = "Terminal-Commands"></a>

This boilerplate comes with pre-configured terminal commands to shorten the long setup process of a new web project. To use them, open a terminal in Visual Studio Code by navigating to Terminal -> New Terminal or, by default, pressing **Ctrl+Shift+`** then entering the command. Depending on your Windows Environment, commands may need to be preceded by the appropriate Windows Environment Variable, usually ``npm``.

Command | Description
--------|------------
start | Run a local Webpack development server.
build | Bundle project files into a distributable form. Similar to compiling.

## Packages ##
<a name = "Packages"></a>

This boilerplate comes with pre-configured packages to shorten the long setup process of a new web project.

Package | Description
--------|------------
aframe | WebXR engine based on Three.js. The core of this boilerplate.
aframe-extras | Extends aframe with useful additions such as playing animations.
@types/aframe | Helps TypeScript understand aframe types and improve intellisense.
@types/html-webpack-plugin | Helps TypeScript understand html-webpack-plugin types and improve intellisense.
@types/three | Helps TypeScript understand three types and improve intellisense.
clean-webpack-plugin | Webpack plugin used to clean the build folder of old files before adding new ones when building project.
css-loader | Webpack plugin used to load CSS files. Works in conjunction with style-loader.
file-loader | Webpack plugin used to load asset files such as GLB and GLTF.
html-loader | Webpack plugin used to load HTML files.
html-webpack-plugin | Webpack plugin used to automatically generate a HTML page to run your WebXR experience.
style-loader | Webpack plugin used to load CSS files. Works in conjunction with css-loader.
ts-loader | Webpack plugin used to load TypeScript files.
typescript | Strongly typed programming language built on JavaScript. Reduces programming mistakes, improves intellisense, and makes programming for web projects easier for programmers with backgrounds in strongly typed programming languages such as C#.
webpack | File bundler with plugin support for web projects. Allows code and assets to be split into separate files and helps optimize references, similar to a compiler.
webpack-cli | Command Line Interface for webpack. Provides useful commands for tasks such as running a local development server.
webpack-dev-server | Development server tool for webpack. Has useful options such as live reloading.

## Helpers ##
<a name = "Helpers"></a>

This boilerplate comes with a library of convenience classes and functions referred to as 'helpers', found in the *src/helpers* folder, to do common tasks or make complex tasks easier. They can be imported into a script by adding the below code to the top of the script.

```typescript
import { name_of_class_or_function } from "relative_file_path_to_src_folder/helpers/helper_file_to_import_from";

E.g.

import { createObject } from "./helpers/createObject";
```

### Classes ###
<a name = "Classes"></a>

Class | File | Description
------|------|------------


### Functions ###
<a name = "Functions"></a>

Function | File | Description
---------|------|------------
createObject | createObject | Add an object as a child of another object and, optionally, give it attrubites.
loadAsset | loadAsset | Load an asset file.

[Back to top](#Technical-Documentation)